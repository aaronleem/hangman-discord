from typing import Dict, List, Union
from discord.ext.commands import Context as DiscordContext, Cog as DiscordCog, command as DiscordCommand
from discord import Embed, Color, Member

from ..database import Statuses, GameType, games as games_db

blue_color = Color.from_rgb(0, 0, 255)

class Stats(DiscordCog):
  def __init__(self, bot):
    self.bot = bot

  @DiscordCommand(brief="Reset your own recorded stats")
  async def reset(self, ctx: DiscordContext):
    await games_db.update_many({
      "player": ctx.author.id
    }, {
      "$set": {
        "player": None
      }
    })

    await games_db.update_many({
      "players.challenger": ctx.author.id
    }, {
      "$set": {
        "players.challenger": None
      }
    })

    await games_db.update_many({
      "players.opponent": ctx.author.id
    }, {
      "$set": {
        "players.opponent": None
      }
    })

    await ctx.reply("Your stats have been reset!", mention_author=False)

  async def get_users_amount_of_games(
    self, player_id, status: Union[Statuses, List[Statuses]],
    game_type: Union[GameType, List[GameType]] = None, extra: Dict = None
  ):
    matcher = {
      "$and": [
        {
          "$or": [
            { "player": player_id },
            { "players.challenger": player_id },
            { "players.opponent": player_id }
          ]
        }
      ]
    }

    if isinstance(status, Statuses):
      matcher["status"] = status.value
    elif isinstance(status, list) and len(status) > 0:
      matcher["$and"].append({
        "$or": [
          { "status": passed_status.value } for passed_status in status
        ]
      })

    if isinstance(game_type, GameType):
      matcher["type"] = game_type.value
    elif isinstance(game_type, list) and len(game_type) > 0:
      matcher["$and"].append({
        "$or": [
          { "type": passed_type.value } for passed_type in game_type
        ]
      })
    else:
      matcher["$and"].append({
        "$or": [
          { "type": GameType.SINGLE_PLAYER.value },
          { "type": GameType.SINGLE_PLAYER_FAST.value },
          { "type": GameType.MULTI_PLAYER.value }
        ]
      })

    matcher = [
      { "$match": matcher }
    ]

    if extra is not None:
      matcher.append(extra)

    result = await games_db.aggregate(matcher).to_list(None)

    return result

  @DiscordCommand()
  async def stats(self, ctx: DiscordContext, member: Member = None):
    member = member or ctx.author

    sp_games = (await self.get_users_amount_of_games(
      member.id,
      [
        Statuses.GAME_WON, Statuses.GAME_LOST, Statuses.GAME_FORFEITED
      ],
      [
        GameType.SINGLE_PLAYER, GameType.SINGLE_PLAYER_FAST
      ],
      extra={
        "$facet": {
          "gamesWon": [
            {
              "$match": {
                "status": Statuses.GAME_WON.value
              }
            }, {
              "$count": "count"
            }
          ],
          "gamesLost": [
            {
              "$match": {
                "status": Statuses.GAME_LOST.value
              }
            }, {
              "$count": "count"
            }
          ],
          "gamesForfeited": [
            {
              "$match": {
                "status": Statuses.GAME_FORFEITED.value
              }
            }, {
              "$count": "count"
            }
          ]
        }
      }
    ))[0]

    mp_games = (await self.get_users_amount_of_games(
      member.id,
      [
        Statuses.GAME_WON, Statuses.GAME_FORFEITED,
        Statuses.INVITE_PENDING, Statuses.INVITE_DECLINED, Statuses.INVITE_TIMED_OUT,
        Statuses.MATCH_COMPLETE
      ],
      GameType.MULTI_PLAYER,
      extra={
        "$facet": {
          "gamesWon": [
            {
              "$match": {
                "status": {
                  "$in": [
                    Statuses.GAME_WON.value, Statuses.GAME_FORFEITED.value
                  ]
                },
                "winner": member.id
              }
            }, {
              "$count": "count"
            }
          ],
          "gamesLost": [
            {
              "$match": {
                "status": Statuses.GAME_WON.value,
                "winner": {
                  "$ne": member.id
                }
              }
            }, {
              "$count": "count"
            }
          ],
          "matchesWon": [
            {
              "$match": {
                "status": Statuses.MATCH_COMPLETE.value,
                "winner": member.id
              }
            }, {
              "$count": "count"
            }
          ],
          "matchesLost": [
            {
              "$match": {
                "status": Statuses.MATCH_COMPLETE.value,
                "winner": {
                  "$ne": member.id
                }
              }
            }, {
              "$count": "count"
            }
          ],
          "gamesForfeited": [
            {
              "$match": {
                "status": Statuses.GAME_FORFEITED.value,
                "winner": {
                  "$ne": member.id
                }
              }
            }, {
              "$count": "count"
            }
          ],
          "invitesSent": [
            {
              "$match": {
                "players.challenger": member.id
              }
            }, {
              "$count": "count"
            }
          ],
          "invitesReceived": [
            {
              "$match": {
                "players.opponent": member.id
              }
            }, {
              "$count": "count"
            }
          ],
          "invitesDeclined": [
            {
              "$match": {
                "status": Statuses.INVITE_DECLINED.value,
                "players.opponent": member.id
              }
            }, {
              "$count": "count"
            }
          ],
          "invitesIgnored": [
            {
              "$match": {
                "status": Statuses.INVITE_TIMED_OUT.value,
                "players.opponent": member.id
              }
            }, {
              "$count": "count"
            }
          ],
        }
      }
    ))[0]

    def safe_count_get(lst):
      try:
        return lst[0]["count"]
      except IndexError:
        return 0

    sp_games_won = safe_count_get(sp_games["gamesWon"])
    sp_games_lost = safe_count_get(sp_games["gamesLost"])
    sp_games_forfeited = safe_count_get(sp_games["gamesForfeited"])
    mp_games_won = safe_count_get(mp_games["gamesWon"])
    mp_games_lost = safe_count_get(mp_games["gamesLost"])
    mp_games_forfeited = safe_count_get(mp_games["gamesForfeited"])
    mp_matches_won = safe_count_get(mp_games["matchesWon"])
    mp_matches_lost = safe_count_get(mp_games["matchesLost"])
    invites_sent = safe_count_get(mp_games["invitesSent"])
    invites_received = safe_count_get(mp_games["invitesReceived"])
    invites_declined = safe_count_get(mp_games["invitesDeclined"])
    invites_ignored = safe_count_get(mp_games["invitesIgnored"])

    stats_embed = Embed()
    stats_embed.title = f"{member.name}'s Hangman stats"
    stats_embed.color = blue_color

    stats_embed.add_field(name="\u200b", value="**Single player\n----------------**", inline=False)
    stats_embed.add_field(name="Games won", value=sp_games_won)
    stats_embed.add_field(name="Games lost", value=sp_games_lost)
    stats_embed.add_field(name="Games forfeited", value=sp_games_forfeited)

    stats_embed.add_field(name="\u200b", value="**Multi player\n--------------**", inline=False)
    stats_embed.add_field(name="Games won", value=mp_games_won)
    stats_embed.add_field(name="Games lost", value=mp_games_lost)
    stats_embed.add_field(name="Games forfeited", value=mp_games_forfeited)
    stats_embed.add_field(name="Matches won", value=mp_matches_won)
    stats_embed.add_field(name="Matches lost", value=mp_matches_lost)
    stats_embed.add_field(name="Invites sent", value=invites_sent)
    stats_embed.add_field(name="Invites received", value=invites_received)
    stats_embed.add_field(name="Invites declined", value=invites_declined)
    stats_embed.add_field(name="Invites ignored", value=invites_ignored)

    stats_embed.add_field(name="\u200b", value="**Total\n--------------**", inline=False)
    stats_embed.add_field(name="Games won", value=sp_games_won + mp_games_won)
    stats_embed.add_field(name="Games lost", value=sp_games_lost + mp_games_lost)
    stats_embed.add_field(name="Games forfeited", value=sp_games_forfeited + mp_games_forfeited)

    await ctx.send(embed=stats_embed)
