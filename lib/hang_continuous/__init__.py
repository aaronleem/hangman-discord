from asyncio import Lock as AsyncLock
from typing import Dict, Union
from discord.ext.commands.bot import AutoShardedBot

from ..database import get_guild_delete_registered_messages, get_continuous_channels, add_continuous_channel, remove_continuous_channel
from discord import TextChannel as DiscordTextChannel
from discord.ext.commands import Cog as DiscordCog

from .game import Hangman as HangmanGame

class HangmanContinuous(DiscordCog):
  def __init__(self, bot):
    self.bot: AutoShardedBot = bot
    self.__channels_in_use: Dict[int, HangmanGame] = {}
    self.__channel_list_lock = AsyncLock()

  @DiscordCog.listener()
  async def on_ready(self):
    channels = await get_continuous_channels()
    for channel in channels:
      self.bot.fetch_channel
      channel_instance = self.bot.get_channel(channel["channelId"])

      if channel_instance is None:
        channel_instance = self.bot.fetch_channel(channel["channelId"])

      if channel_instance is None:
        continue

      if not isinstance(channel_instance, DiscordTextChannel):
        continue

      await self.start_game(channel_instance)

  async def game_in_progress(self, channel: DiscordTextChannel):
    async with self.__channel_list_lock:
      return channel.id in self.__channels_in_use

  async def complete_game(self, channel: DiscordTextChannel):
    async with self.__channel_list_lock:
      hangman_game: Union[HangmanGame, None] = self.__channels_in_use.get(channel.id, None)
      if hangman_game is None:
        return

      del self.__channels_in_use[channel.id]

    hangman_game.complete()
    hangman_game.remove_all_listeners("game_complete")

    await self.start_game(channel)

  async def start_game(self, channel: DiscordTextChannel):
    async with self.__channel_list_lock:
      if self.__channels_in_use.get(channel.id, None) is not None:
        game = self.__channels_in_use[channel.id]
        await game.abort()
        game.complete()
        game.remove_all_listeners("game_complete")

        del self.__channels_in_use[channel.id]
        await channel.send("Continuous game has been stopped!")
        return False

      guild_delete_registered_messages = await get_guild_delete_registered_messages(channel.guild.id)
      self.__channels_in_use[channel.id] = hangman_game = HangmanGame(self.bot, channel, guild_delete_registered_messages)

    hangman_game.get_word()
    hangman_game.setup_message_event()

    hangman_game.add_listener("game_complete", self.complete_game)
    await hangman_game.start()

    return True

  async def play_continuous_game(self, channel: DiscordTextChannel):
    if await self.start_game(channel) is True:
      await add_continuous_channel(channel.id)
    else:
      await remove_continuous_channel(channel.id)
