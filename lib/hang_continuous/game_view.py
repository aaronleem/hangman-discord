import discord
from string import ascii_lowercase

class GameView(discord.ui.View):
  def __init__(self, available_letters, letter_page, callback):
    super().__init__(timeout=None)

    def letter_handler(btn):
      async def callback(interaction):
        await self.letter_handler(btn, interaction)
      return callback

    self.available_letters = available_letters
    self.letter_page = letter_page
    self.callback = callback

    letter_rows = 0

    max_in_row = 4 + self.letter_page

    ascii_from = 0 if self.letter_page == 0 else (4 * 3)

    counter = 0
    for char in ascii_lowercase[ascii_from:]:
      if counter == max_in_row:
        letter_rows += 1
        counter = 0

      if letter_rows == 3:
        break

      counter += 1

      btn = discord.ui.Button(label=char.upper(), style=discord.ButtonStyle.blurple, disabled=char not in available_letters, row=letter_rows)
      btn.callback = letter_handler(btn)
      self.add_item(btn)

    btn = discord.ui.Button(label="More Letters", style=discord.ButtonStyle.green, row=letter_rows + 1)
    btn.callback = letter_handler(btn)
    self.add_item(btn)

  async def letter_handler(self, button: discord.ui.Button, interaction: discord.Interaction):
    if button.disabled:
      return

    await self.callback(button, interaction)
