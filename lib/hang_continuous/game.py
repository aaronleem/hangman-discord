from asyncio import ensure_future as EnsureFuture

from discord import Button
from discord.enums import InteractionType
from discord.interactions import Interaction
from .game_view import GameView
from typing import List, Union
from discord.channel import TextChannel
from discord.threads import Thread
from .. import HangmanBot
from discord import Embed, Color, Message, HTTPException
import aiohttp
from eventemitter.emitter import EventEmitter
from ..random_words import RandomWords
from ..hang.word import Word

hang = [
  ' +---+',
  ' |   |',
  '     |',
  '     |',
  '     |',
  '     |',
  '======='
]

man = [
  ['     |'],
  [' 0   |'],
  [' 0   |', ' |   |'],
  [' 0   |', '/|   |'],
  [' 0   |', '/|\\  |'],
  [' 0   |', '/|\\  |', '/    |'],
  [' 0   |', '/|\\  |', '/ \\  |']
]

random_word = RandomWords()
random_word.shuffle_words.start()

red_color = Color.from_rgb(255, 0, 0)
green_color = Color.from_rgb(0, 255, 0)

class Hangman(EventEmitter):
  def __init__(self, bot, channel, delete_registered_messages):
    super().__init__()
    self.__bot: HangmanBot = bot
    self.__channel: Union[TextChannel, Thread] = channel

    self.__delete_registered_messages: bool = delete_registered_messages

    self.__word: str = None
    self.__word_instance: Word = None
    self.__word_result: dict = None
    self.__message: Message = None
    self.__button_letter_page: int = 0

    self.__message_event_index: str = None
    self.__guesses: List[str] = []

    EnsureFuture(self.get_definiton())

  async def get_definiton(self):
    try:
      async with aiohttp.ClientSession() as session:
        async with session.get(f'https://api.dictionaryapi.dev/api/v2/entries/en/{self.__word}') as resp:
          try:
            self.__word_result = (await resp.json())[0]
          except (IndexError, KeyError):
            pass
    except:
      pass # ignore failures

  def get_word(self, max_letter_length=None):
    word = random_word.random_words(None, max_letter_length)[0]

    if word == None:
      return False

    if len(word) < 6:
      return self.get_word()

    self.__word = word
    self.__word_instance = Word(word.lower())

    return True

  def solve_word(self):
    self.__word_instance.solve()

  def setup_message_event(self):
    self.__message_event_index = self.__bot.register_message_handler(self.on_message)

  def guess(self, word):
    self.__guesses.append(word.upper())
    return self.__word_instance.guess_letter(word.lower())

  def end_game(self):
    self.emit("game_complete", self.__channel)

  async def win_game(self, last_guesser: str):
    await self.update_message(embed=self.generate_embed(last_guesser, True), view=[])
    self.end_game()

  async def on_interaction(self, button: Button, interaction: Interaction):
    if interaction.type != InteractionType.component:
      return

    if interaction.user is None or interaction.message is None or self.__message is None:
      return

    if interaction.message.id != self.__message.id:
      return

    if interaction.response.is_done():
      return

    if button.label == "More Letters":
      self.__button_letter_page = 1 if self.__button_letter_page == 0 else 0
      await self.update_message(embed=self.generate_embed(), view=self.generate_letter_button_map())
      await interaction.response.defer()
      return

    self.guess(button.label)

    if self.__word_instance.guessed:
      await interaction.response.defer()
      return await self.win_game(interaction.user.mention)

    await self.update_message(embed=self.generate_embed(), view=self.generate_letter_button_map())
    await interaction.response.defer()

  async def update_message(self, *args, **kwargs):
    try:
      await self.__message.edit(*args, **kwargs)
    except HTTPException:
      self.__message = await self.__channel.send(*args, **kwargs)

  async def on_message(self, message: Message):
    if (
      message.author.bot or
      message.channel.id != self.__channel.id or
      len(message.content) != 1
    ):
      return

    if message.guild is not None and self.__delete_registered_messages:
      permissions = message.channel.permissions_for(message.guild.me)
      if getattr(permissions, "send_messages") == True:
        await message.delete()

    guess_status = self.guess(message.content)

    if guess_status == -1:
      return await self.__channel.send(f"The letter `{message.content.upper()}` has already been guessed!")

    if self.__word_instance.guessed:
      return await self.win_game(message.author.mention)

    await self.update_message(embed=self.generate_embed(), view=self.generate_letter_button_map())

  def generate_letter_button_map(self):
    return GameView(self.__word_instance.available_letters, self.__button_letter_page, self.on_interaction)

  def generate_embed(self, last_guesser: str = None, game_over: bool = False):
    circle_count = ' '.join([
      f":regional_indicator_{ltr}:"
      if ltr != '*'
      else ":blue_circle:"
      for _, ltr in enumerate(self.__word_instance.displayed_word)
    ])

    game_embed = Embed()
    game_embed.title = "Continuous Hangman"
    game_embed.color = green_color

    if game_over:
      game_embed.add_field(
        name="Game over!",
        value=None if last_guesser is None else f"{last_guesser} guessed the word!",
        inline=False
      )

    if len(self.__guesses) > 0:
      game_embed.add_field(name="Guesses", value=f"`{''.join(self.__guesses)}`", inline=False)

    if game_over and self.__word_result is not None:
      try:
        phoenetics = self.__word_result['phonetics']
        phoenetic_list = []

        for phoenetic in phoenetics:
          phoenetic_list.append(f"[{phoenetic['text']}]({phoenetic['audio']})")

        circle_count += f"\n\nPronounced: {'; '.join(phoenetic_list)}"
      except (IndexError, KeyError):
        pass

    game_embed.add_field(name=f"Word ({len(self.__word_instance.displayed_word)})", value=circle_count, inline=False)

    if game_over and self.__word_result is not None:
      try:
        meanings = self.__word_result['meanings']

        for meaning in meanings:
          definition = ""
          def_count = 0
          for defined in meaning["definitions"]:
            if def_count == 3 or len(definition) > 1024:
              break

            def_count += 1

            definition += f"- {defined['definition']}\n"

          game_embed.add_field(name=meaning["partOfSpeech"], value=definition, inline=False)
      except (IndexError, KeyError):
        pass

    return game_embed

  def complete(self):
    self.__bot.deregister_message_handler(self.__message_event_index)

  async def start(self):
    self.__message = await self.__channel.send(embed=self.generate_embed(), view=self.generate_letter_button_map())

  async def abort(self):
    self.solve_word()
    await self.update_message(embed=self.generate_embed(None, True), view=[])
    self.end_game()
