from asyncio import Event as AsyncEvent, wait_for as AsyncWaitFor, Lock as AsyncLock, TimeoutError, CancelledError, wait as AsyncWait, FIRST_COMPLETED
from lib.hang.views import MultiplayerInviteView

from discord.ext.commands import Context as DiscordContext
from discord import Message as DiscordMessage
from discord import Member as DiscordMember
from .game import Hangman as HangmanGame

from typing import TypedDict, Union

class Invite(TypedDict):
  initiator: bool
  opposition: str
  timer: AsyncEvent
  message: Union[None, DiscordMessage]

class InvalidLengthError(BaseException):
  pass

class Controller:
  def __init__(self):
    self.__games = {}
    self.__channels_currently_having_a_game = []
    self.__pending_invites = {}
    self.__invite_lock = AsyncLock()
    self.__game_lock = AsyncLock()
    self.__channel_lock = AsyncLock()

  @property
  def game_count(self):
    return len(self.__games)

  async def create_invite(self, initiator, opponent):
    pending_invite_event = AsyncEvent()

    async with self.__invite_lock:
      self.__pending_invites[initiator.id] = {"initiator": True, "opposition": opponent.id, "timer": pending_invite_event, "message": None}
      self.__pending_invites[opponent.id] = {"initiator": False, "opposition": initiator.id, "timer": pending_invite_event, "message": None}

    return pending_invite_event

  async def wait_for_300_seconds(self, invite, message):
    try:
      await AsyncWaitFor(invite["timer"].wait(), 300)
    except CancelledError:
      pass
    except TimeoutError:
      await message.edit("Invite timed out and cancelled!", components=[])
      return False

  async def wait_for_accept_invite(self, ctx, mp_invite_view: MultiplayerInviteView, message: DiscordMessage):
    initiator = ctx.author
    initiator_invite = self.get_invite_instance(initiator)
    opponent_invite = self.get_invite_instance(initiator_invite["opposition"])

    initiator_invite["message"] = opponent_invite["message"] = message

    done, pending = await AsyncWait(
      {
        self.wait_for_300_seconds(initiator_invite, message),
        mp_invite_view.wait()
      }, return_when=FIRST_COMPLETED
    )

    pending.pop().cancel()
    done.pop().result()

    await self.invite_action(ctx.author, mp_invite_view.accepted is False)

    if mp_invite_view.accepted is False:
      return False

    async def invalidate_invite():
      async with self.__invite_lock:
        del self.__pending_invites[initiator.id]
        del self.__pending_invites[initiator_invite["opposition"]]

    return invalidate_invite

  def get_invite_instance(self, of_member) -> Invite:
    return self.__pending_invites[of_member.id if isinstance(of_member, DiscordMember) else of_member]

  def get_invited_member(self, of_member):
    return self.get_invite_instance(of_member)["opposition"]

  async def invite_action(self, of_member, cancel=False):
    opposing_id = self.get_invite_instance(of_member)

    if cancel:
      async with self.__invite_lock:
        del self.__pending_invites[of_member.id]
        del self.__pending_invites[opposing_id["opposition"]]

    opposing_id["timer"].set()

  def is_invite_initiator(self, member):
    return self.__pending_invites[member.id]["initiator"]

  def has_pending_invite(self, member):
    return member.id in self.__pending_invites.keys()

  def currently_in_game(self, member):
    return member.id in self.__games.keys()

  def currently_has_game(self, channel):
    return channel.id in self.__channels_currently_having_a_game

  async def quit_game(self, member):
    hangman_game = self.__games[member.id]
    await hangman_game.abort(member)

  async def attempt_guess(self, member, word):
    hangman_game = self.__games[member.id]
    await hangman_game.guess_word(member, word)

  async def book_channel(self, channel):
    async with self.__channel_lock:
      self.__channels_currently_having_a_game.append(channel.id)

  async def clear_channel(self, channel):
    async with self.__channel_lock:
      self.__channels_currently_having_a_game.remove(channel.id)

  async def play_game(self, game_id, match_id, bot, ctx, channel, fast, participants, games_won, max_letter_length, game_number, timeout, delete_registered_messages: bool):
    hangman_game = HangmanGame(game_id, bot, ctx.send if not isinstance(ctx, DiscordContext) else None, channel, game_number, fast, timeout, delete_registered_messages, games_won=games_won, *participants)

    async with self.__game_lock:
      for participant in participants:
        self.__games[participant.id] = hangman_game

    try:
      valid_word = hangman_game.get_word(max_letter_length)

      if not valid_word:
        raise InvalidLengthError
    except ValueError as e:
      msg, = e.args

      async with self.__game_lock:
        for participant in participants:
          del self.__games[participant.id]

      if msg == 'Param "max_length" must be greater than 5.' or msg == 'Param "count" must be less than 1. (It is only 1 words)':
        raise InvalidLengthError

    await hangman_game.start()
    hangman_game.setup_message_event()

    await hangman_game.complete()

    async with self.__game_lock:
      for participant in participants:
        del self.__games[participant.id]

    return hangman_game.result
