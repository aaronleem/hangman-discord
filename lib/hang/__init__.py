from lib.hang.views import AlreadyInvitedView, MultiplayerInviteView
import re
from typing import Any, Callable, Coroutine, Union
from discord.ext.commands.bot import AutoShardedBot

from discord import Member as DiscordMember
from discord.ext.commands import Cog as DiscordCog
from discord.ext.commands.context import Context as DiscordCommandContext
from discord.ext.commands import command as DiscordCommand
from discord.ext.commands import group as DiscordGroup
from discord.ext.commands import Converter as DiscordConverter
from discord.ext.commands import bot_has_permissions as DiscordBotHasPermissions
from discord.ext.commands import has_permissions as DiscordMemberHasPermissions

from ..util import member_to_json

from .controller import Controller as GameController, InvalidLengthError, Invite

from ..database import GameType, Statuses
from ..database import new_game, update_game
from ..database import set_guild_steal_timeout, get_guild_steal_timeout
from ..database import set_guild_delete_registered_messages, get_guild_delete_registered_messages

from ..hang_continuous import HangmanContinuous

from uuid import uuid4

import logging
logger = logging.getLogger('hangman')

best_of_re = r"^(?:bo)?(\d+)$"

class BestOf(DiscordConverter):
  async def convert(self, ctx, argument):
    if argument.isnumeric():
      return int(argument)

    results = re.findall(best_of_re, argument)

    if len(results) == 0:
      return None

    return int(results[0])

class Hangman(DiscordCog, name="Hangman"):
  bot: AutoShardedBot
  __continuous_cog: HangmanContinuous

  def __init__(self, bot):
    self.bot = bot
    self.controller = GameController()
    self.games_won = {}

  @DiscordCog.listener()
  async def on_ready(self):
    self.__continuous_cog = self.bot.get_cog("HangmanContinuous")
  
  async def valid_play_conditions(self, ctx, max_letter_length, member, best_of, multiplayer, game_id, logger_prefix):
    if multiplayer:
      if member is None:
        logger.info("Failed: No opponent", extra={logger_prefix: game_id})
        await ctx.send("You need to mention someone to play against!")
        return False

      if member.bot:
        logger.info("Failed: Bot opponent", extra={logger_prefix: game_id})
        await ctx.send("Pff, you can't multi a bot. That's suicide! Have you never played Counter Strike?!")
        return False

      if member.id == ctx.author.id:
        logger.info("Failed: Self opponent", extra={logger_prefix: game_id})
        await ctx.send("You played yo'self, fool")
        return False

      if best_of is not None:
        if best_of < 1:
          logger.info("Failed: Best Of too low", extra={logger_prefix: game_id})
          await ctx.send("`Best of` needs to be above 0")
          return False

        if best_of % 2 == 0:
          logger.info("Failed: Best Of invalid", extra={logger_prefix: game_id})
          await ctx.send("`Best of` needs to be an odd number")
          return False

    if self.controller.currently_has_game(ctx.channel):
      logger.info("Failed to play hangman: Channel already has game", extra={logger_prefix: game_id})
      await ctx.send("There's already a game of hangman being played in here")
      return False

    if self.controller.currently_in_game(ctx.author):
      logger.info("Failed to play hangman: User already playing game", extra={logger_prefix: game_id})
      await ctx.send("You're already playing a game of Hangman buddy")
      return False

    if multiplayer and self.controller.currently_in_game(member):
      logger.info("Failed: Member already playing a game", extra={logger_prefix: game_id})
      await ctx.send("That member is already playing a game of Hangman")
      return False

    if not multiplayer and max_letter_length is not None and max_letter_length < 6:
      logger.info("Failed to play hangman: Max letter length invalid", extra={logger_prefix: game_id, "requestedLength": max_letter_length})
      await ctx.send("That's an invalid letter length!")
      return False

    if self.controller.has_pending_invite(ctx.author):
      logger.info("Failed: Actor has pending invite", extra={logger_prefix: game_id})
      invite: Invite = self.controller.get_invite_instance(ctx.author)
      opposing_member = await ctx.guild.fetch_member(
        self.controller.get_invited_member(ctx.author)
      )

      if self.controller.is_invite_initiator(ctx.author):
        await ctx.send(
          f"You've already invited {opposing_member.mention} to play!",
          view=AlreadyInvitedView(invite["message"])
        )
      else:
        await ctx.send(f"{opposing_member.mention} has already invited you to play!")

      return False

    if multiplayer:
      if self.controller.has_pending_invite(member):
        logger.info("Failed: Opponent has pending invite", extra={logger_prefix: game_id})
        opposing_member = await ctx.guild.fetch_member(
          self.controller.get_invited_member(member)
        )

        await ctx.send(f"{opposing_member.mention} has already been invited to play!")

        return False

    return True

  @DiscordGroup(brief="Sets options for your guild")
  @DiscordMemberHasPermissions(manage_guild=True)
  async def set(self, ctx):
    pass

  @set.command(brief="Set your guild's steal timeout. Shows timeout if no argument presented.", name="steal_timeout")
  @DiscordBotHasPermissions(send_messages=True)
  @DiscordMemberHasPermissions(manage_guild=True)
  async def set_steal_timeout(self, ctx, timeout: int = None):
    if(timeout == None):
      return await ctx.send(f"Current steal timeout: {(await get_guild_steal_timeout(ctx.guild.id)) or 30} seconds")

    await set_guild_steal_timeout(ctx.guild.id, timeout)
    await ctx.send(f"Set steal timeout to {timeout} seconds")

  @set.command(brief="Sets bot to delete registered guesses in your guild. Shows value if no argument presented.", name="delete_registered_messages")
  @DiscordBotHasPermissions(send_messages=True)
  @DiscordMemberHasPermissions(manage_guild=True)
  async def set_delete_registered_messages(self, ctx, value: str = None):
    if(value == None):
      current_value = await get_guild_delete_registered_messages(ctx.guild.id)
      option = "Deleting" if current_value else "Not deleting"

      return await ctx.send(f"Current setting: {option} registered guesses")

    if value not in ["yes", "no"]:
      return await ctx.send(f"Invalid value `{value}`; needs to be `yes` or `no`")

    option = "deleting" if value == "yes" else "not deleting"
    await set_guild_delete_registered_messages(ctx.guild.id, value == "yes")
    await ctx.send(f"Now {option} your registered guesses")

  @DiscordGroup(brief="Starts a continuous hangman game", invoke_without_command=True)
  @DiscordBotHasPermissions(send_messages=True)
  @DiscordMemberHasPermissions(manage_guild=True)
  async def continuous(self, ctx):
    await self.__continuous_cog.play_continuous_game(ctx.channel)

  @DiscordGroup(brief="Starts a single-player hangman game", invoke_without_command=True)
  @DiscordBotHasPermissions(send_messages=True)
  async def play(self, ctx, max_letter_length: int = None):
    try:
      await self.play_game(ctx, max_letter_length)
    except InvalidLengthError:
      await ctx.send(f"{max_letter_length} is an invalid word length!")

  @DiscordCommand(brief="Starts an endless single-player hangman game")
  @DiscordBotHasPermissions(send_messages=True)
  async def endless(self, ctx: DiscordCommandContext, *, max_letter_length: int = None):
    await self.play_game(ctx, max_letter_length, endless=True)

  @DiscordCommand(brief="Starts a fast-paced single-player hangman game (15 sec guess)")
  @DiscordBotHasPermissions(send_messages=True)
  async def fast(self, ctx, max_letter_length: int = None):
    try:
      await self.play_game(ctx, max_letter_length, fast=True)
    except InvalidLengthError:
      await ctx.send(f"{max_letter_length} is an invalid word length!")

  async def play_game(self, ctx, max_letter_length, fast=False, endless=False):
    game_type = GameType.SINGLE_PLAYER if not fast else GameType.SINGLE_PLAYER_FAST

    while True:
      game_id = str(uuid4())

      logger.info("Attempting to play a Hangman game", extra={
        "gameId": game_id,
        "type": game_type.name,
        "member": member_to_json(ctx.author),
        "endless": endless,
        "fast": fast
      })

      if not await self.valid_play_conditions(ctx, max_letter_length, None, None, False, game_id, "gameId"):
        return

      logger.info("Inserting new game record into database", extra={"gameId": game_id})
      await new_game(game_id,
        game_type=game_type,
        status=Statuses.GAME_IN_PROGRESS,
        player=ctx.author.id
      )

      guild_timeout = (await get_guild_steal_timeout(ctx.guild.id)) or 30
      guild_delete_registered_messages = await get_guild_delete_registered_messages(ctx.guild.id)

      logger.info("Booking channel for game", extra={"gameId": game_id, "channelId": ctx.channel.id})
      await self.controller.book_channel(ctx.channel)

      logger.info("Playing Hangman game", extra={"gameId": game_id})
      try:
        results = await self.controller.play_game(
          game_id=game_id,
          match_id=None,
          bot=self.bot,
          channel=ctx.channel,
          fast=fast,
          participants=[ctx.author],
          games_won=None,
          max_letter_length=max_letter_length,
          game_number=None,
          timeout=guild_timeout,
          delete_registered_messages=guild_delete_registered_messages,
          ctx=ctx
        )
      except InvalidLengthError:
        await ctx.send(f"{max_letter_length} is an invalid word length!")

        await update_game(game_id,
          status=Statuses.GAME_FORFEITED
        )

        logger.info("Clearing booked channel", extra={"gameId": game_id, "channelId": ctx.channel.id})
        await self.controller.clear_channel(ctx.channel)

        return

      logger.info("Game complete; updating game record in database", extra={
        "gameId": game_id,
        "result": (
          Statuses.GAME_WON if results.winner else
          Statuses.GAME_FORFEITED if results.forfeit else
          Statuses.GAME_LOST
        ).name
      })

      await update_game(game_id,
        status=(
          Statuses.GAME_WON if results.winner else
          Statuses.GAME_FORFEITED if results.forfeit else
          Statuses.GAME_LOST
        )
      )

      logger.info("Clearing booked channel", extra={"gameId": game_id, "channelId": ctx.channel.id})
      await self.controller.clear_channel(ctx.channel)

      logger.info("Completed game successfully", extra={"gameId": game_id})

      if(results.forfeit != None or not endless):
        break

  @DiscordCommand(brief="Quits an existing game of hangman")
  @DiscordBotHasPermissions(send_messages=True)
  async def quit(self, ctx):
    if not self.controller.currently_in_game(ctx.author):
      return await ctx.send("You're not currently in a game of Hangman")

    await self.controller.quit_game(ctx.author)

  @DiscordCommand(brief="Guess the whole word")
  @DiscordBotHasPermissions(send_messages=True)
  async def guess(self, ctx, word: str):
    if not self.controller.currently_in_game(ctx.author):
      return await ctx.send("You're not currently in a game of Hangman")

    await self.controller.attempt_guess(ctx.author, word)

  @DiscordCommand(brief="Cancels a pending invite", hidden=True)
  @DiscordBotHasPermissions(send_messages=True)
  async def cancel(self, ctx):
    """
      [DEPRECATED] Cancel or decline a multi-player hangman game.
    """
    await ctx.send(f"This command has been deprecated. Please use the buttons that appear when using `{ctx.prefix}multi`.")

  @DiscordCommand(brief="Accepts a pending invite", hidden=True)
  @DiscordBotHasPermissions(send_messages=True)
  async def accept(self, ctx):
    """
      [DEPRECATED] Accept a multi-player hangman game.
    """
    await ctx.send(f"This command has been deprecated. Please use the buttons that appear when using `{ctx.prefix}multi`.")

  @play.command(brief="Starts a multi-player hangman game", name="vs")
  @DiscordBotHasPermissions(send_messages=True)
  async def play_vs(self, ctx, member: DiscordMember = None, best_of: BestOf = None):
    await self.multiplayer(ctx, member, best_of)

  @DiscordCommand(brief="Starts a multi-player hangman game")
  @DiscordBotHasPermissions(send_messages=True)
  async def multi(self, ctx, member: DiscordMember = None, best_of: BestOf = None):
    await self.multiplayer(ctx, member, best_of)

  async def multiplayer(self, ctx: DiscordCommandContext, member, best_of):
    match_id = str(uuid4())

    logger_cat = "gameId" if best_of is None else "matchId"

    logger.info("Attempting to play a Hangman game", extra={logger_cat: match_id, "type": "MP", "member": ctx.author.id, "bestOf": best_of})

    if not await self.valid_play_conditions(ctx, None, member, best_of, True, match_id, logger_cat):
      return

    logger.info("Pre-check complete; creating invite", extra={logger_cat: match_id})

    await self.controller.create_invite(ctx.author, member)

    logger.info("Inserting new game record into database", extra={logger_cat: match_id})

    if best_of is not None:
      await new_game(None, match_id,
        game_type=GameType.MULTI_PLAYER_MATCH,
        status=Statuses.INVITE_PENDING,
        initiator=ctx.author.id,
        opposition=member.id
      )
    else:
      await new_game(match_id,
        game_type=GameType.MULTI_PLAYER,
        status=Statuses.INVITE_PENDING,
        initiator=ctx.author.id,
        opposition=member.id
      )

    mp_invite_view = MultiplayerInviteView(opponent=member, initiator=ctx.author)
    message = await ctx.send(
      f"{member.mention}, you have been invited to play Hangman. Invite will automatically cancel in 5 minutes.",
      view=mp_invite_view
    )

    logger.info("Waiting for invite to be accepted", extra={logger_cat: match_id})
    accepted_invite: Union[Callable[[], Coroutine[Any, Any, None]], bool] = await self.controller.wait_for_accept_invite(ctx, mp_invite_view, message)
    if accepted_invite is False:
      await update_game(match_id, status=Statuses.INVITE_DECLINED)
      return

    if not self.controller.has_pending_invite(ctx.author) and not self.controller.has_pending_invite(member):
      logger.info("Game has been cancelled", extra={logger_cat: match_id})
      await update_game(match_id, status=Statuses.INVITE_DECLINED)
      return # cancelled above

    await accepted_invite() # wait_for_accept_invite returns a function to invalidate the invite, and we can start the game below

    # Re-validate play conditions
    if not await self.valid_play_conditions(ctx, None, member, best_of, True, match_id, logger_cat):
      await update_game(match_id, status=Statuses.INVITE_DECLINED)
      return

    await update_game(match_id, status=Statuses.GAME_IN_PROGRESS if best_of is None else Statuses.MATCH_IN_PROGRESS)


    guild_timeout = (await get_guild_steal_timeout(ctx.guild.id)) or 30
    guild_delete_registered_messages = await get_guild_delete_registered_messages(ctx.guild.id)

    if best_of is None:
      logger.info("Starting game", extra={
        "gameId": match_id
      })

      logger.info("Booking channel for game", extra={"gameId": match_id, "channelId": ctx.channel.id})
      await self.controller.book_channel(ctx.channel)

      logger.info("Playing Hangman game", extra={"gameId": match_id})
      results = await self.controller.play_game(
        game_id=match_id,
        match_id=None,
        bot=self.bot,
        channel=ctx.channel,
        fast=False,
        participants=[ctx.author, member],
        games_won=None,
        max_letter_length=None,
        game_number=None,
        timeout=guild_timeout,
        delete_registered_messages=guild_delete_registered_messages,
        ctx=ctx
      )

      logger.info("Game complete; updating game record in database", extra={
        "gameId": match_id,
        "results": {
          "status": (
            Statuses.GAME_FORFEITED if results.forfeit else
            Statuses.GAME_WON
          ).name,
          "winner": results.winner.id,
          "loser": results.loser.id if results.loser else None,
          "forfeit": results.forfeit.id if results.forfeit else None
        }
      })

      await update_game(match_id,
        winner=results.winner.id,
        status=(
          Statuses.GAME_FORFEITED if results.forfeit else
          Statuses.GAME_WON
        )
      )

      logger.info("Clearing booked channel", extra={"gameId": match_id, "channelId": ctx.channel.id})
      await self.controller.clear_channel(ctx.channel)

      logger.info("Completed game successfully", extra={"gameId": match_id})
      return

    if self.games_won.get(match_id) is None:
      self.games_won[match_id] = [0, 0]

    logger.info("Starting match", extra={logger_cat: match_id})

    await ctx.send(f"Creating a Best Of {best_of} between {ctx.author.mention} and {member.mention}! Please wait..")

    logger.info("Booking channel for match", extra={logger_cat: match_id})
    await self.controller.book_channel(ctx.channel)

    forfeit = False
    forfeit_info = {
      "count": 0,
      "winner_id": "",
      "winner_position": -1
    }

    logger.info(f"Starting best of {best_of}", extra={logger_cat: match_id})
    for game_number in range(1, best_of + 1):
      game_id = str(uuid4())

      logger.info("Inserting new game record into database", extra={"matchId": match_id, "gameId": game_id})

      await new_game(game_id, match_id,
        game_type=GameType.MULTI_PLAYER,
        status=Statuses.GAME_IN_PROGRESS,
        initiator=ctx.author.id,
        opposition=member.id
      )

      if forfeit != False:
        forfeit_info["count"] += 1

        await update_game(game_id,
          winner=forfeit_info["winner_id"],
          status=Statuses.GAME_FORFEITED
        )
        continue

      logger.info(f"Playing Hangman game", extra={"matchId": match_id, "gameId": game_id, "game_number": game_number})
      results = await self.controller.play_game(
        game_id=game_id,
        match_id=match_id,
        bot=self.bot,
        channel=ctx.channel,
        fast=False,
        participants=[ctx.author, member],
        games_won=self.games_won[match_id],
        max_letter_length=None,
        game_number=game_number,
        timeout=guild_timeout,
        delete_registered_messages=guild_delete_registered_messages,
        ctx=ctx
      )

      logger.info("Game complete; updating game record in database", extra={
        "matchId": match_id, "gameId": game_id,
        "results": {
          "status": (
            Statuses.GAME_FORFEITED if results.forfeit else
            Statuses.GAME_WON
          ).name,
          "winner": results.winner.id,
          "loser": results.loser.id if results.loser else None,
          "forfeit": results.forfeit.id if results.forfeit else None
        }
      })

      await update_game(game_id,
        winner=results.winner.id,
        status=(
          Statuses.GAME_FORFEITED if results.forfeit else
          Statuses.GAME_WON
        )
      )

      if results.forfeit is not None:
        forfeit = True
        forfeit_info = {"count": 1, "winner_id": results.winner.id, "winner_position": 0 if results.winner.id == ctx.author.id else 1}
      else:
        self.games_won[match_id][0 if results.winner.id == ctx.author.id else 1] += 1

      await ctx.send(f"Round over! {results.winner.name} took this round!")

    logger.info("Match complete; updating match in database", extra={
      logger_cat: match_id,
      "results": {
        "challenger_wins": self.games_won[match_id][0],
        "opponent_wins": self.games_won[match_id][1],
        "forfeited": forfeit
      }
    })

    await update_game(match_id, True,
      winner=(ctx.author if self.games_won[match_id][0] > self.games_won[match_id][1] else member).id,
      status=Statuses.MATCH_COMPLETE
    )

    if forfeit != False:
      self.games_won[match_id][forfeit_info["winner_position"]] += forfeit_info["count"]

    result  = f"{ctx.author.mention} {self.games_won[match_id][0]}"
    result += " - "
    result += f"{self.games_won[match_id][1]} {member.mention}"

    if forfeit == True:
      ffid = forfeit_info["winner_id"]
      result += f" (last {forfeit_info['count']} rounds forfeited by {ctx.author.mention if ffid != ctx.author.id else member.mention})"
      result += f"\n\n{(ctx.author if ffid == ctx.author.id else member).mention} wins the game by forfeit!"
    else:
      result += f"\n\n{(ctx.author if self.games_won[match_id][0] > self.games_won[match_id][1] else member).mention} wins the game!"

    await ctx.send(f"GAME OVER! Here are the results:\n{result}")

    logger.info("Match complete, clearing booked channel", extra={logger_cat: match_id})
    await self.controller.clear_channel(ctx.channel)
    del self.games_won[match_id]

    logger.info("Completed match successfully", extra={logger_cat: match_id})
