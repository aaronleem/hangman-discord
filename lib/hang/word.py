"""
  Serves as the base of any hangman word
  Accepts word as a constructing argument
"""

from string import ascii_lowercase

class Word:
  def __init__(self, word):
    self.word = word.lower()
    self.__validate()

    self.displayed_word = '*' * len(word)
    self.__letters_available = list(ascii_lowercase)

  def __validate(self):
    for _, ltr in enumerate(self.word):
      if ltr not in ascii_lowercase:
        raise TypeError(f"Invalid character in word: `{ltr}`")

  def solve(self):
    self.displayed_word = self.word

  @property
  def guessed(self):
    return self.displayed_word == self.word

  @property
  def available_letters(self):
    return self.__letters_available

  def guess_letter(self, letter):
    if letter not in self.__letters_available:
      return -1

    self.__letters_available.remove(letter)

    indexes = [i for i, ltr in enumerate(self.word) if ltr == letter]

    if len(indexes) == 0:
      return 0

    for index in indexes:
      self.displayed_word = self.displayed_word[0:index] + letter + self.displayed_word[index+1:]

    return 1

  def guess_word(self, word):
    if self.word != word:
      return {"status": 0}

    for letter in word:
      try:
        self.__letters_available.remove(letter)
      except ValueError:
        pass # raises when letter has already been removed

    return {"status": 1}
