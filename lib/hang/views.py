import discord
from string import ascii_lowercase
from discord.enums import ButtonStyle

from discord.member import Member
from discord.message import Message
from discord.ui.button import Button

class AlreadyInvitedView(discord.ui.View):
  def __init__(self, invite_message: Message):
    super().__init__()

    guild_id = invite_message.guild.id
    channel_id = invite_message.channel.id
    message_id = invite_message.id

    self.add_item(Button(label="Go to invite", style=ButtonStyle.url, url=f"https://discord.com/channels/{guild_id}/{channel_id}/{message_id}"))

class MultiplayerInviteView(discord.ui.View):
  def __init__(self, initiator: Member, opponent: Member):
    super().__init__(timeout=None)

    self.__opponent = opponent
    self.__initiator = initiator
    self.__accepted = False

  @property
  def accepted(self):
    return self.__accepted

  async def interaction_check(self, interaction: discord.Interaction) -> bool:
      return await super().interaction_check(interaction)

  @discord.ui.button(label='Accept Invite', style=discord.ButtonStyle.green)
  async def accept(self, button: discord.ui.Button, interaction: discord.Interaction):
    if interaction.message is None or interaction.user is None:
      return

    if interaction.user.id != self.__initiator.id and interaction.user.id != self.__opponent.id:
      return await interaction.response.send_message(content="You cannot accept someone else's invite!", ephemeral=True)

    if interaction.user.id == self.__initiator.id:
      return await interaction.response.send_message(content="You cannot accept your own invite!", ephemeral=True)

    await interaction.response.defer()
    await interaction.message.edit(content="Invite accepted! Good Luck and Have Fun!", view=None)

    self.__accepted = True

    self.stop()

  @discord.ui.button(label='Decline/Cancel Invite', style=discord.ButtonStyle.red)
  async def decline(self, button: discord.ui.Button, interaction: discord.Interaction):
    if interaction.message is None or interaction.user is None:
      return

    if interaction.user.id != self.__initiator.id and interaction.user.id != self.__opponent.id:
      return await interaction.response.send_message(content="You cannot cancel someone else's invite!", ephemeral=True)

    await interaction.response.defer()

    if interaction.user.id == self.__opponent.id:
      await interaction.message.edit(content="Invite declined! Try again another time :(", view=None)
    else:
      await interaction.message.edit(content="Invite cancelled!", view=None)

    self.stop()

class GameView(discord.ui.View):
  def __init__(self, available_letters, letter_page, is_multiplayer, callback):
    super().__init__(timeout=None)

    def letter_handler(btn):
      async def callback(interaction):
        await self.letter_handler(btn, interaction)
      return callback

    self.is_multiplayer = is_multiplayer
    self.available_letters = available_letters
    self.letter_page = letter_page
    self.callback = callback

    letter_rows = 0

    max_in_row = 4 + self.letter_page

    ascii_from = 0 if self.letter_page == 0 else (4 * 3)

    counter = 0
    for char in ascii_lowercase[ascii_from:]:
      if counter == max_in_row:
        letter_rows += 1
        counter = 0

      if letter_rows == 3:
        break

      counter += 1

      btn = discord.ui.Button(label=char.upper(), style=discord.ButtonStyle.blurple, disabled=char not in available_letters, row=letter_rows)
      btn.callback = letter_handler(btn)
      self.add_item(btn)

    btn = discord.ui.Button(label="More Letters", style=discord.ButtonStyle.green, row=letter_rows + 1)
    btn.callback = letter_handler(btn)
    self.add_item(btn)

    btn = discord.ui.Button(label="Quit Game" if not self.is_multiplayer else "Forfeit Match", style=discord.ButtonStyle.red, row=letter_rows + 1)
    btn.callback = letter_handler(btn)
    self.add_item(btn)

  async def letter_handler(self, button: discord.ui.Button, interaction: discord.Interaction):
    if button.disabled:
      return

    await self.callback(button, interaction)
