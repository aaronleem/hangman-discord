from asyncio import Event as AsyncEvent, ensure_future as EnsureFuture, sleep as AsyncSleep
from datetime import timedelta
from .. import HangmanBot
from types import SimpleNamespace
from discord import Embed, Color, Message, HTTPException, Interaction, InteractionType, Button
import aiohttp
from discord.member import Member
from ..random_words import RandomWords
from .word import Word
from ..util import member_to_json
import logging
from .views import GameView

from lib.timeout import Timeout

logger = logging.getLogger('hangman')

hang = [
  ' +---+',
  ' |   |',
  '     |',
  '     |',
  '     |',
  '     |',
  '======='
]

man = [
  ['     |'],
  [' 0   |'],
  [' 0   |', ' |   |'],
  [' 0   |', '/|   |'],
  [' 0   |', '/|\\  |'],
  [' 0   |', '/|\\  |', '/    |'],
  [' 0   |', '/|\\  |', '/ \\  |']
]

random_word = RandomWords()
random_word.shuffle_words.start()

red_color = Color.from_rgb(255, 0, 0)
green_color = Color.from_rgb(0, 255, 0)

class Hangman:
  def __init__(self, internal_id, bot, send_method, channel, game_number, fast, timeout, delete_registered_messages, *participants, games_won=None, secondary_id=None):
    if 0 < len(participants) > 2:
      raise ValueError("Invalid amount of participants; must be 1-2")

    self.__bot: HangmanBot = bot
    self.__game_id = internal_id if secondary_id is None else secondary_id
    self.__match_id = internal_id if secondary_id is not None else None
    self.__games_won = games_won
    self.__channel = channel
    self.__fast = None if not fast else Timeout(15)
    self.__guess_timeout = Timeout(timedelta(minutes=15))

    self.__steal_available = False
    self.__steal_timeout: Timeout = None if len(participants) == 1 else Timeout(timeout)
    self.__steal_attempted: List[int] = []

    self.__delete_registered_messages = delete_registered_messages

    self.__participants = participants if game_number is None or (game_number is not None and game_number % 2 == 1) else participants[::-1]
    self.__participantIds = list(participant.id for participant in self.__participants)
    self.__word = None
    self.__word_instance: Word = None
    self.__word_result: dict = None
    self.__message: Message = None
    self.__send_method = send_method

    self.__game_end_event = AsyncEvent()
    self.__message_event_index = None
    self.__guesses = []
    self.__button_letter_page = 0

    self.__turn = 0
    self.__max_lives = 6
    self.__lives = 0

    self.__game_number = game_number

    self.__winner = None
    self.__loser = None
    self.__forfeit = None

    EnsureFuture(self.get_definiton())

  def loggerer(self, message, extra={}):
    extra_info = {
      "gameId": self.__game_id
    }

    if self.__match_id is not None:
      extra_info.update({
        "matchId": self.__match_id
      })

    extra_info.update(extra)
    logger.info(message, extra=extra_info)

  async def get_definiton(self):
    try:
      async with aiohttp.ClientSession() as session:
        async with session.get(f'https://api.dictionaryapi.dev/api/v2/entries/en/{self.__word}') as resp:
          try:
            self.__word_result = (await resp.json())[0]
          except (IndexError, KeyError):
            pass
    except:
      pass # ignore failures

  @property
  def result(self):
    return SimpleNamespace(
      winner=self.__winner,
      loser=self.__loser,
      forfeit=self.__forfeit
    )

  def multiplayer(self):
    return len(self.__participants) > 1

  def alive(self):
    return self.__lives < self.__max_lives

  def turn(self, invert=False):
    if invert:
      return 0 if self.__turn == 1 else 1
    return self.__turn

  def get_participant(self, index, opposing=False) -> Member:
    if index != 0 and index != 1:
      index = self.__participantIds.index(index)

    if opposing:
      index = 0 if index == 1 else 1

    return self.__participants[index]

  def get_word(self, max_letter_length=None):
    word = random_word.random_words(None, max_letter_length)[0]

    if word == None:
      return False

    if len(word) < 6:
      return self.get_word()

    self.__word = word
    self.__word_instance = Word(word.lower())

    return True

  def solve_word(self):
    self.__word_instance.solve()

  def setup_message_event(self):
    self.__message_event_index = self.__bot.register_message_handler(self.on_message)

  def guess(self, word):
    self.__guesses.append(word.upper())
    return self.__word_instance.guess_letter(word.lower())

  def incorrect_guess(self):
    if self.multiplayer():
      self.__turn = self.turn(True)
      return

    self.__lives += 1

    if not self.alive():
      self.solve_word()

  def end_game(self):
    self.__game_end_event.set()

  async def win_game(self):
    winner = True

    if self.multiplayer():
      self.__winner = self.get_participant(self.turn())
      self.__loser = self.get_participant(self.turn(True))
    else:
      winner = self.alive()

      if self.__fast is not None:
        self.__fast.stop()

      if winner:
        self.__winner = self.get_participant(self.turn())
      else:
        self.__loser = self.get_participant(self.turn())

    await self.update_message(embed=self.generate_embed((self.__winner or self.__loser).name, winner), view=[])
    self.end_game()

  async def guess_word(self, guesser: Member, word: str):
    self.loggerer("Attempted to guess word", extra={
      "guesser": member_to_json(guesser),
      "word": self.__word,
      "guess": word
    })

    self.__guess_timeout.stop()

    steal_attempt = False

    if self.multiplayer():
      self.loggerer("Checking if steal attempt..")

      current_participant = self.get_participant(self.turn())
      steal_attempt = current_participant.id != guesser.id

      if not steal_attempt:
        self.loggerer("Not steal attempt")
      else:
        if not self.__steal_available:
          self.loggerer("Steal attempted while not available")
          return

        self.loggerer("Steal available; performing switch logic")

        self.__turn = self.turn(True)

    guess_status = self.__word_instance.guess_word(word)

    if guess_status["status"] == 0:
      self.loggerer("Incorrect guess")

      if not self.multiplayer():
        self.incorrect_guess()
      else:
        self.loggerer("Switching turn")

        self.__turn = self.turn(True)

        self.__steal_timeout.stop()

        if steal_attempt:
          if guesser.id not in self.__steal_attempted:
            self.loggerer("Appended member to steal attempted list, resetting steal waiter", extra={
              "member": member_to_json(guesser)
            })

            self.__steal_attempted.append(guesser.id)

        if not self.__word_instance.guessed:
          self.__steal_timeout.start()

    if guess_status["status"] == 1:
      self.loggerer("Correct guess")
      self.solve_word()

    if self.__word_instance.guessed:
      self.loggerer("Winning game..")

      return await self.win_game()

    self.__guess_timeout.start()

    self.loggerer("Updating embed..")
    await self.update_message(embed=self.generate_embed(), view=self.generate_letter_button_map())

  def is_button_forfeit(self):
    return self.__games_won is not None

  async def on_interaction(self, button: Button, interaction: Interaction):
    if interaction.type != InteractionType.component:
      return

    if interaction.user is None or interaction.message is None or self.__message is None:
      return

    if interaction.message.id != self.__message.id:
      return

    if interaction.response.is_done():
      return

    if interaction.user.id not in self.__participantIds:
      await interaction.response.send_message("You can't play in someone else's game! Shame on you >:(", ephemeral=True)
      return

    if (interaction.user.id != self.__participantIds[self.__turn]):
      await interaction.response.send_message("You can't play when it's not your turn!", ephemeral=True)
      return

    if button.label == "More Letters":
      self.__button_letter_page = 1 if self.__button_letter_page == 0 else 0
      await self.update_message(embed=self.generate_embed(), view=self.generate_letter_button_map())
      await interaction.response.defer()
      return

    if button.label == "Quit Game" or button.label == "Forfeit Match":
      await self.abort(interaction.user)
      await interaction.response.defer()
      return

    guess_status = self.guess(button.label)

    self.__guess_timeout.stop()

    if self.__fast is not None:
      self.__fast.stop()

    if guess_status == 0:
      self.incorrect_guess()

    if self.multiplayer():
      self.__steal_timeout.stop()

      if not self.__word_instance.guessed:
        self.__steal_timeout.start()
        self.__steal_available = False

    if self.__word_instance.guessed:
      await interaction.response.defer()
      return await self.win_game()

    self.__guess_timeout.start()

    await self.update_message(embed=self.generate_embed(), view=self.generate_letter_button_map())
    await interaction.response.defer()

  async def update_message(self, *args, **kwargs):
    try:
      await self.__message.edit(*args, **kwargs)
    except (HTTPException, AttributeError):
      self.__message = await self.__channel.send(*args, **kwargs)

  async def on_message(self, message: Message):
    if (
      message.author.bot or
      message.channel.id != self.__channel.id or
      len(message.content) != 1 or
      message.author.id not in self.__participantIds or
      message.author.id != self.__participantIds[self.__turn]
    ):
      return

    if message.guild is not None and self.__delete_registered_messages:
      permissions = message.channel.permissions_for(message.guild.me)
      if getattr(permissions, "send_messages") == True:
        await message.delete()

    guess_status = self.guess(message.content)

    if guess_status == -1:
      return await self.__channel.send(f"The letter `{message.content.upper()}` has already been guessed!")

    self.__guess_timeout.stop()

    if self.__fast is not None:
      self.__fast.stop()

    if guess_status == 0:
      self.incorrect_guess()

    if self.multiplayer():
      self.__steal_timeout.stop()

      if not self.__word_instance.guessed:
        self.__steal_timeout.start()
        self.__steal_available = False

    if self.__word_instance.guessed:
      return await self.win_game()

    self.__guess_timeout.start()

    await self.update_message(embed=self.generate_embed(), view=self.generate_letter_button_map())

  def generate_letter_button_map(self):
    return GameView(available_letters=self.__word_instance.available_letters, letter_page=self.__button_letter_page, is_multiplayer=self.__game_number is not None, callback=self.on_interaction)

  def generate_embed(self, player_name=None, winner=None, has_timed_out=False):
    circle_count = ' '.join([
      f":regional_indicator_{ltr}:"
      if ltr != '*'
      else ":blue_circle:"
      for _, ltr in enumerate(self.__word_instance.displayed_word)
    ])

    game_embed = Embed()
    game_embed.title = self.get_participant(0).name
    game_embed.color = red_color if self.__steal_available else green_color
    game_embed.set_footer(text=f"Game ID: {self.__game_id}")

    if self.multiplayer():
      game_embed.title += f" vs {self.get_participant(1).name}"

      if self.__game_number is not None:
        game_embed.title += f": game {self.__game_number}"

    if player_name is not None:
      game_embed.add_field(
        name=(
          "Forfeit by timeout!" if winner == -1 and has_timed_out else
          "Forfeited!" if winner == -1 else
          "Winner!" if winner else
          "Loser!"
        ),
        value=player_name,
        inline=False
      )

    if len(self.__guesses) > 0:
      game_embed.add_field(name="Guesses", value=f"`{''.join(self.__guesses)}`", inline=False)

    if player_name is not None and self.__word_result is not None:
      try:
        phoenetics = self.__word_result['phonetics']
        phoenetic_list = []

        for phoenetic in phoenetics:
          phoenetic_list.append(f"[{phoenetic['text']}]({phoenetic['audio']})")

        circle_count += f"\n\nPronounced: {'; '.join(phoenetic_list)}"
      except (IndexError, KeyError):
        pass

    game_embed.add_field(name=f"Word ({len(self.__word_instance.displayed_word)})", value=circle_count, inline=False)

    if player_name is not None and self.__word_result is not None:
      try:
        meanings = self.__word_result['meanings']

        for meaning in meanings:
          definition = ""
          def_count = 0
          for defined in meaning["definitions"]:
            if def_count == 3 or len(definition) > 1024:
              break

            def_count += 1

            definition += f"- {defined['definition']}\n"

          game_embed.add_field(name=meaning["partOfSpeech"], value=definition, inline=False)
      except (IndexError, KeyError):
        pass

    if self.multiplayer() and player_name is None:
      game_embed.add_field(name="Turn", value=self.get_participant(self.turn()).mention, inline=False)

      participants = self.__participants if self.__game_number is None or (self.__game_number is not None and self.__game_number % 2 == 1) else self.__participants[::-1]

      if self.__games_won is not None:
        game_embed.add_field(name="Games won", value=(
          f"{participants[0].mention}: {self.__games_won[0]}\n"
          f"{participants[1].mention}: {self.__games_won[1]}"
        ), inline=False)

    if not self.multiplayer() and player_name is None:
      height = 2
      hangman_figure = hang[:]

      our_man = man[self.__lives]

      for j in range(len(our_man)):
        hangman_figure[height+j] = our_man[j]

      hangman_figure = '\n'.join(hangman_figure)
      game_embed.description = f"```{hangman_figure}```"

    if not self.multiplayer() and player_name is None and self.__fast is not None:
      self.__fast.start()

    return game_embed
  
  async def fast_timeout(self):
    self.__lives = self.__max_lives
    self.solve_word()
    await self.win_game()

  async def guess_timeout(self):
    await self.abort(self.get_participant(self.turn()))

  async def steal_timeout(self):
    self.__steal_available = True
    await self.update_message(embed=self.generate_embed(), view=self.generate_letter_button_map())

  async def complete(self):
    self.loggerer("Waiting for game to complete..")

    await self.__game_end_event.wait()

    self.__bot.deregister_message_handler(self.__message_event_index)

    self.__guess_timeout.remove_all_listeners("timeout")

    if self.__fast is not None:
      self.__fast.remove_all_listeners("timeout")

    self.loggerer("Game has completed!")

  async def start(self):
    if self.__send_method is not None:
      self.__message = await self.__send_method(embed=self.generate_embed(), view=self.generate_letter_button_map())
    else:
      self.__message = await self.__channel.send(embed=self.generate_embed(), view=self.generate_letter_button_map())

    self.__guess_timeout.add_listener("timeout", self.guess_timeout)

    if self.__fast is not None:
      self.__fast.add_listener("timeout", self.fast_timeout)

    if self.multiplayer():
      self.__steal_timeout.add_listener("timeout", self.steal_timeout)

    await AsyncSleep(0.2) # slight delay to prevent misfiring
    self.__guess_timeout.start()

    if self.multiplayer():
      self.__steal_timeout.start()

  async def abort(self, author):
    self.solve_word()
    self.__forfeit = self.get_participant(author.id)

    if len(self.__participantIds) > 1:
      self.__winner = self.get_participant(author.id, True)

    await self.update_message(embed=self.generate_embed(author.name, -1, False), view=[])
    self.end_game()
