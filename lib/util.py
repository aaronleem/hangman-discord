import discord

def member_to_json(member: discord.Member):
  return {
    "id": member.id,
    "nick": member.display_name,
    "tag": f"{member.name}#{member.discriminator}"
  }
