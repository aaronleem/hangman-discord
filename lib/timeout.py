from asyncio import Event as AsyncEvent, create_task as CreateTask, wait_for as WaitFor
from asyncio.exceptions import TimeoutError as AsyncTimeout
from asyncio.tasks import Task

from eventemitter import EventEmitter

from typing import Union
from datetime import timedelta

class Timeout(EventEmitter):
  __timer: AsyncEvent
  __waiter: Task
  __timeout: float

  def __init__(self, timeout: Union[float, timedelta]) -> None:
    super().__init__()
    self.__timer = AsyncEvent()
    self.__waiter = None

    if isinstance(timeout, timedelta):
      self.__timeout = timeout.total_seconds()
    else:
      self.__timeout = timeout

  async def __timeout_waiter(self):
    try:
      await WaitFor(self.__timer.wait(), self.__timeout)
    except AsyncTimeout:
      self.emit("timeout")

  def start(self):
    waiter = CreateTask(self.__timeout_waiter())

    if self.__waiter == None or self.__waiter.cancelled:
      self.__waiter = waiter
    
    self.emit("started")

  def stop(self):
    if not self.__waiter.done():
      self.__timer.set()
      self.__waiter.cancel()

    self.__timer.clear()

    self.emit("stopped")

  def reset(self):
    self.stop()

    self.emit("reset")

    self.start()
