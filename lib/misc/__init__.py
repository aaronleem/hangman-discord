from discord.ext.commands import AutoShardedBot as DiscordBot, Context as DiscordContext, Cog as DiscordCog, command as DiscordCommand
from discord import Embed, Game, utils, Permissions as DiscordPermissions, AllowedMentions as DiscordAllowedMentions, File as DiscordFile
from discord.ext import tasks

from ..hang import Hangman

from datetime import datetime, timedelta
import os
import psutil
import time
import timeago

class Misc(DiscordCog):
  def __init__(self, bot: DiscordBot):
    self.bot: DiscordBot = bot
    self.process = psutil.Process(os.getpid())
    self.__bootup = datetime.now()
    self.__last_update: datetime = datetime(1970,1,1)

    self.update_status.start()

  @property
  def ram_usage(self):
    return self.process.memory_full_info().rss / 1024**2

  @property
  def uptime(self):
    return timeago.format(datetime.now() - self.__bootup)

  @property
  def total_member_count(self):
    return sum(g.member_count for g in self.bot.guilds)

  @property
  def server_count(self):
    return len(self.bot.guilds)

  @property
  def games_in_progress(self):
    HangmanCog: Hangman = self.bot.get_cog("Hangman")
    return HangmanCog.controller.game_count

  @tasks.loop(seconds=15)
  async def update_status(self):
    await self.bot.change_presence(
      activity=Game(
        name=(
          f"Hangman in {self.server_count} guild{'' if self.server_count == 1 else 's'}, "
          f"and {self.total_member_count} player{'' if self.total_member_count == 1 else 's'}!"
          # f"and {self.games_in_progress} game{'' if self.games_in_progress == 1 else 's'} being played!"
        )
      )
    )

  @update_status.before_loop
  async def before_update_status(self):
      print('waiting for task...')
      await self.bot.wait_until_ready()

  def cog_unload(self):
    self.before_update_status.cancel()

  @DiscordCommand(brief="Pong!")
  async def ping(self, ctx: DiscordContext):
    before = time.monotonic()
    before_ws = int(round(self.bot.latency * 1000, 1))
    message = await ctx.reply("🏓 Pong", mention_author=False)
    ping = (time.monotonic() - before) * 1000
    await message.edit(content=f"🏓 WS: {before_ws}ms  |  REST: {int(ping)}ms", allowed_mentions=DiscordAllowedMentions(replied_user=False))

  @DiscordCommand(brief="Display information about the bot")
  async def info(self, ctx):
    owner = await self.bot.fetch_user(232191905394327562)

    embedColour = Embed.Empty
    if hasattr(ctx, 'guild') and ctx.guild is not None:
      embedColour = ctx.me.top_role.colour

    embed = Embed(colour=embedColour)
    embed.set_thumbnail(url=ctx.bot.user.display_avatar.url)
    embed.add_field(name="Last boot", value=self.uptime, inline=False)
    embed.add_field(name="Developer", value=owner, inline=False)
    embed.add_field(name="Library", value="discord.py", inline=False)
    embed.add_field(name="Servers", value=f"{self.server_count} ( avg: {self.total_member_count / self.server_count:,.2f} users/server )", inline=False)
    embed.add_field(name="Games being played", value=self.games_in_progress, inline=False)
    embed.add_field(name="Commands loaded", value=len([x.name for x in self.bot.commands]), inline=False)
    embed.add_field(name="RAM", value=f"{self.ram_usage:.2f} MB", inline=False)

    await ctx.reply(content=f"About **{ctx.bot.user}**", embed=embed, mention_author=False)
  
  @DiscordCommand(brief="Generate an invite link for the bot")
  async def invite(self, ctx: DiscordContext):
    perms = DiscordPermissions()

    perms.send_messages = \
    perms.read_messages = \
    perms.read_message_history = \
    perms.use_slash_commands = \
    perms.use_threads = \
    perms.manage_threads = \
    perms.manage_messages = True

    link = utils.oauth_url(self.bot.user.id, permissions=perms, scopes=["bot", "applications.commands"])

    await ctx.reply(f"Here's an invite link for the bot: {link}", mention_author=False)
  
  # Get meme'd on
  @DiscordCommand(hidden=True, enabled=False)
  async def ban(self, ctx):
    with open(os.path.join(os.path.dirname(__file__), "../../resources/images/bonk/bonk.jfif"), "rb") as fp:
      await ctx.send(file=DiscordFile(fp, "bonk.jpg"))

  @ban.error
  async def ban_error(self, ctx):
    pass
