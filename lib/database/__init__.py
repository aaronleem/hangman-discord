import os

import motor.motor_asyncio
from motor.core import Database, Collection
from typing import List, TypedDict, Union

from .constants import Statuses, GameType

write_stats = os.getenv("STATS", "false") == "true"

class DbChannel(TypedDict):
  channelId: str

client: motor.motor_asyncio.AsyncIOMotorClient = None
db: Database = None
players: Collection = None
games: Collection = None
guilds: Collection = None
continuous_games: Collection = None

async def setup():
  global client, db, players, games, guilds, continuous_games

  client = motor.motor_asyncio.AsyncIOMotorClient(
    host=os.getenv("MONGO_URI", "mongodb://localhost:27017")
  )
  db = client.hangman
  players = db.players
  games = db.games
  guilds = db.guilds
  continuous_games = db.continuous_games

  await players.create_index("discordId", name="discordId", unique=True)
  await games.create_index("gameId", name="gameId", unique=False)
  await guilds.create_index("guildId", name="guildId", unique=False)
  await continuous_games.create_index("channelId", name="channelId", unique=False)

async def set_guild_steal_timeout(guild_id, steal_timeout):
  await guilds.find_one_and_update({
    "guildId": guild_id
  }, {
    "$set": {
      "timeout": steal_timeout
    }
  }, upsert=True)

async def set_guild_delete_registered_messages(guild_id, delete_registered_messages):
  await guilds.find_one_and_update({
    "guildId": guild_id
  }, {
    "$set": {
      "delete_registered_messages": delete_registered_messages
    }
  }, upsert=True)

async def get_guild_steal_timeout(guild_id) -> int:
  guild = await guilds.find_one({
    "guildId": guild_id
  })

  if guild is None:
    return 30

  return guild.get("timeout", 30)

async def get_guild_delete_registered_messages(guild_id) -> bool:
  guild = await guilds.find_one({
    "guildId": guild_id
  })

  if guild is None:
    return False

  return guild and guild.get("delete_registered_messages", False)

async def get_continuous_channels() -> List[DbChannel]:
  items = continuous_games.find()

  return await items.to_list(None)

async def add_continuous_channel(channel_id) -> None:
  await continuous_games.insert_one({
    "channelId": channel_id
  })

async def remove_continuous_channel(channel_id) -> None:
  await continuous_games.find_one_and_delete({
    "channelId": channel_id
  })

async def new_game(game_id, match_id=None, *, game_type: GameType, status: Statuses, player=None, initiator=None, opposition=None, winner=None, series: int = None, count: int = None):
  if not write_stats:
    return

  game_object = {
    "gameId": game_id,
    "status": status.value,
    "type": game_type.value,
  }

  if player:
    game_object.update({
      "player": player
    })
  else:
    game_object.update({
      "players": {
        "challenger": initiator,
        "opponent": opposition,
      }
    })

  if match_id:
    game_object.update({
      "matchId": match_id
    })

  if game_id:
    game_object.update({
      "gameId": game_id
    })

  await games.insert_one(game_object)

async def update_game(game_id, match=False, *, status: Statuses, winner=None, count: int = None):
  if not write_stats:
    return

  updated_game_object = {
    "status": status.value
  }

  if winner is not None:
    updated_game_object.update({
      "winner": winner
    })

  await games.find_one_and_update({
    ("gameId" if match == False else "matchId"): game_id
  }, {
    "$set": updated_game_object
  })

async def fetch_game_by_game_id(game_id):
  found_games = games.find({
    "gameId": game_id
  }, {
    "_id": 0
  })

  return await found_games.to_list(None)

async def fetch_games_by_player_id(player_id, status: Union[Statuses, List[Statuses]] = []):
  found_games = games.find({
    "$or": [
      { "player": player_id },
      { "players.challenger": player_id },
      { "players.opponent": player_id }
    ]
  }, {
    "_id": 0
  })

  return await found_games.to_list(None)

async def fetch_games_by_status(status: Statuses):
  found_games = games.find({
    "status": status.value
  }, {
    "_id": 0
  })

  return await found_games.to_list(None)
