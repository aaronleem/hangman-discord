from enum import Enum, auto

class GameType(Enum):
  SINGLE_PLAYER = auto()
  SINGLE_PLAYER_FAST = auto()
  MULTI_PLAYER = auto()
  MULTI_PLAYER_MATCH = auto() # best_of


class Statuses(Enum):
  INVITE_PENDING = auto()
  INVITE_DECLINED = auto()
  INVITE_TIMED_OUT = auto()
  GAME_IN_PROGRESS = auto()
  GAME_WON = auto()
  GAME_LOST = auto()
  GAME_LOST_TIMEOUT = auto()
  GAME_FORFEITED = auto()
  MATCH_IN_PROGRESS = auto()
  MATCH_COMPLETE = auto()
