# -*- coding: utf-8 -*-

import pytest
import random

from .. import RandomWords


class TestsRandomWords:

    @classmethod
    def setup_class(cls):
        cls.rw = RandomWords()
        cls.letters = 'qwertyuiopasdfghjklzcvbnm'

    @pytest.mark.asyncio
    async def test_random_word(self):
        await self.rw.shuffle_words()

        for letter in self.letters:
            word = self.rw.random_word(letter)
            assert word[0] == letter

    @pytest.mark.asyncio
    async def test_random_word_value_error(self):
        await self.rw.shuffle_words()

        pytest.raises(ValueError, self.rw.random_word, 'x')
        pytest.raises(ValueError, self.rw.random_word, 0)
        pytest.raises(ValueError, self.rw.random_word, -1)
        pytest.raises(ValueError, self.rw.random_word, 9)
        pytest.raises(ValueError, self.rw.random_word, ['u'])
        pytest.raises(ValueError, self.rw.random_word, 'fs')

    @pytest.mark.asyncio
    async def test_random_words(self):
        await self.rw.shuffle_words()

        for letter in self.letters:
            words = self.rw.random_words(letter)
            for word in words:
                assert word[0] == letter

    @pytest.mark.asyncio
    async def test_random_words_value_error(self):
        await self.rw.shuffle_words()

        pytest.raises(ValueError, self.rw.random_words, 'fa')
        pytest.raises(ValueError, self.rw.random_words, ['fha'])
        pytest.raises(ValueError, self.rw.random_words, 0)
        pytest.raises(ValueError, self.rw.random_words, -1)

        pytest.raises(ValueError, self.rw.random_words, letter=None,
                      count=1000000)

        pytest.raises(ValueError, self.rw.random_words, count=0)
        pytest.raises(ValueError, self.rw.random_words, count=None)
        pytest.raises(ValueError, self.rw.random_words, count=[8])
        pytest.raises(ValueError, self.rw.random_words, count=-5)

    @pytest.mark.asyncio
    async def test_random_words_length_list(self):
        await self.rw.shuffle_words()

        len_words = len(self.rw.random_words())
        assert len_words == 1

        len_words = len(self.rw.random_words(count=10))
        assert len_words == 10

        len_words = len(self.rw.random_words(count=73))
        assert len_words == 73

        for letter in self.letters:
            len_random = random.randint(1, 3)
            len_words = len(self.rw.random_words(letter, count=len_random))
            assert len_words == len_random

        len_random = 1000000
        for letter in self.letters:
            pytest.raises(
                ValueError, self.rw.random_words, letter, count=len_random)
