# -*- coding: utf-8 -*-

import os
import json
from random import randint, sample
from itertools import chain
from discord.ext import tasks

main_dir = os.path.dirname(os.path.abspath(__file__))


class Random(dict):
    def __init__(self, file):
        self.types_file = {
            'words': self.load_words,
            'ihl_words': self.load_words
        }

        self.load_file(file)

        super().__init__()

    def load_file(self, file):
        """
        :param str file: filename
        """
        self.types_file[file](file)

    def load_words(self, file):
        """
        Load dict from file for random words.

        :param str file: filename
        """
        with open(os.path.join(main_dir, file + '.dat'), 'r') as f:
            self.words = json.load(f)

    @staticmethod
    def check_count(count):
        """
        Checks count

        :param int count: count number ;)
        :raises: ValueError
        """
        if type(count) is not int:
            raise ValueError('Param "count" must be int.')
        if count < 1:
            raise ValueError('Param "count" must be greater than 0.')

    def shuffle(self, entries, limit=-1):
      if limit != -1:
        limit = limit // 1

      working_array = entries[:]

      currentIndex = len(working_array) - 1
      randomIndex = None

      while currentIndex > 0:
        randomIndex = randint(0, currentIndex)
        currentIndex -= 1

        tempVal = working_array[currentIndex]
        working_array[currentIndex] = working_array[randomIndex]
        working_array[randomIndex] = tempVal

      if limit != -1:
        removal = len(entries) - limit

        while removal > 0:
          removal -= 1

          randomIndex = randint(0, currentIndex)
          del working_array[randomIndex]

      return working_array


class RandomWords(Random):

    def __init__(self):
        self.available_letters = 'qwertyuiopasdfghjklzcvbnm'
        self.shuffled_list = []

        super().__init__(os.getenv("WORDLIST", 'words'))

    @tasks.loop(minutes=30)
    async def shuffle_words(self):
      wordlist = list(chain.from_iterable(self.words.values()))
      self.shuffled_list = self.shuffle(wordlist, len(wordlist) * .75)

    def random_word(self, letter=None):
        """
        Return random word.

        :param str letter: letter
        :rtype: str
        :returns: random word
        """
        return self.random_words(letter)[0]

    def random_words(self, letter=None, max_length=None, count=1):
        """
        Returns list of random words.

        :param str letter: letter
        :param int max_length: how long words should be
        :param int count: how much words
        :rtype: list
        :returns: list of random words
        :raises: ValueError
        """
        self.check_count(count)

        words = []

        if letter is None:
            all_words = self.shuffled_list

            if max_length is not None:
                if type(max_length) is not int:
                    raise ValueError('Param "max_length" must be int.')
                if max_length < 6:
                    raise ValueError('Param "max_length" must be greater than 5.')

                all_words = list(filter(lambda w: len(w) == max_length, all_words))

            try:
                words = sample(all_words, count)
            except ValueError:
                len_sample = len(all_words)
                raise ValueError('Param "count" must be less than {0}. (It is only {0} words)'.format(len_sample + 1))

        elif type(letter) is not str:
            raise ValueError('Param "letter" must be string.')

        elif letter not in self.available_letters:
            raise ValueError(
                'Param "letter" must be in {0}.'.format(
                    self.available_letters))

        elif letter in self.available_letters:
            try:
                words = sample(self.shuffle(self.words[letter]), count)
            except ValueError:
                len_sample = len(self.words[letter])
                raise ValueError('Param "count" must be less than {0}. (It is only {0} words for letter "{1}")'.format(len_sample + 1, letter))

        return words
