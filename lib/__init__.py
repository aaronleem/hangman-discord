import uuid

from discord.ext.commands import AutoShardedBot as DiscordBot
from discord.ext.commands import CommandInvokeError

import sys
import traceback
import logging
logger = logging.getLogger('hangman')

class HangmanBot(DiscordBot):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.__button_handlers = {}
    self.__message_handlers = {}

  def register_message_handler(self, handler):
    handler_id = uuid.uuid4()
    self.__message_handlers[handler_id] = handler
    return handler_id

  def deregister_message_handler(self, index):
    self.__message_handlers.pop(index)

  def register_interaction_handler(self, handler):
    handler_id = uuid.uuid4()
    self.__button_handlers[handler_id] = handler
    return handler_id

  def deregister_button_handler(self, index):
    self.__button_handlers.pop(index)

  async def on_shard_ready(self, shard_id):
    print(f"Shard {shard_id} ready!")

  async def on_command_error(self, ctx, error):
    if isinstance(error, CommandInvokeError):
      original_error = error.original
      formatted_exception = traceback.format_exception(type(original_error), error, None)

      status = f"{type(original_error).__name__} caught"

      if str(original_error) != "":
        status += f": {str(original_error)}"

      logger.exception(status, extra={
        "traceback": [line.strip("\n") for line in formatted_exception]
      })
    else:
      raise error

  async def on_error(self, evt, *args, **kwargs):
    excType, excValue, excTraceback = sys.exc_info()

    formatted_exception = traceback.format_exception(excType, excValue, excTraceback)

    status = f"{excType.__name__} caught"

    if str(excValue) != "":
      status += f": {str(excValue)}"

    logger.exception(status, extra={
      "traceback": [line.strip("\n") for line in formatted_exception]
    })

  async def on_message(self, message):
    await super().on_message(message)

    for handler in list(self.__message_handlers.values()):
      await handler(message)

  async def on_interaction(self, interaction):
    for handler in list(self.__button_handlers.values()):
      await handler(interaction)
