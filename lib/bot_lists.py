import os
import dbl
from discord.ext import commands
import logging

logger = logging.getLogger('hangman')


class BotList(commands.Cog):
  """Handles interactions with the top.gg API"""

  def __init__(self, bot):
    self.bot = bot
    self.token = os.getenv('DBL_TOKEN', 'Token Not found')
    self.dblpy = dbl.DBLClient(self.bot, self.token, autopost=True) # Autopost will post your guild count every 30 minutes

  @commands.Cog.listener()
  async def on_guild_post(self):
    logger.debug("Server count posted successfully")
