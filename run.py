import os
import logging
import sys

import json_log_formatter

formatter = json_log_formatter.JSONFormatter()

handler = logging.StreamHandler(sys.stdout)
handler.setFormatter(formatter)

logger = logging.getLogger('hangman')
logger.addHandler(handler)

log_level_name = os.getenv("LOG_LEVEL", "")

if logging._nameToLevel.get(log_level_name) != None:
  log_level = logging._nameToLevel[log_level_name]
else:
  log_level = logging.INFO

logger.setLevel(log_level)

import os
from lib import database
from lib import HangmanBot

prefix_list = os.getenv('PREFIX_LIST', 'h/,H/')

client = HangmanBot(command_prefix=prefix_list.split(","), case_insensitive=True)

client.loop.run_until_complete(database.setup())

from lib.hang import Hangman
from lib.hang_continuous import HangmanContinuous
from lib.bot_lists import BotList
from lib.stats import Stats
from lib.misc import Misc

client.add_cog(Hangman(client))
client.add_cog(HangmanContinuous(client))
client.add_cog(BotList(client))
client.add_cog(Stats(client))
client.add_cog(Misc(client))

client.run(os.getenv('BOT_TOKEN', 'Token Not found'))
