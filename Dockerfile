# Use latest python
FROM python:3.9-slim
LABEL Hangmang <its_me@aaronleem.co.za>

WORKDIR /usr/src/app

COPY Pipfile* ./

RUN pip install pipenv

RUN pipenv lock --requirements > requirements.txt
RUN set -ex \
	\
	&& savedAptMark="$(apt-mark showmanual)" \
	&& apt-get update && apt-get install -y --no-install-recommends \
		dpkg-dev \
		gcc \
    git \
		libbluetooth-dev \
		libbz2-dev \
		libc6-dev \
		libexpat1-dev \
		libffi-dev \
		libgdbm-dev \
		liblzma-dev \
		libncursesw5-dev \
		libreadline-dev \
		libsqlite3-dev \
		libssl-dev \
		make \
		tk-dev \
		uuid-dev \
		wget \
		xz-utils \
		zlib1g-dev \
		$(command -v gpg > /dev/null || echo 'gnupg dirmngr') \
	\
  && pip install --target=./ --no-cache-dir -r requirements.txt \
  && pip install --target=./ --no-cache-dir --upgrade git+https://github.com/gear4s/ratelimiter.git@master \
	&& apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
	&& rm -rf /var/lib/apt/lists/*

COPY run.py ./
COPY lib ./lib/
COPY resources ./resources/

CMD ["python", "-u", "./run.py"]
