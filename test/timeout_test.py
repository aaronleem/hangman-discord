import pytest
import time
from datetime import timedelta

from asyncio import Event as AsyncEvent

from lib.timeout import Timeout

class TestTimeout:
  @pytest.mark.timeout(2)
  @pytest.mark.asyncio
  async def test_timeout(self):
    instance = Timeout(1)
    set_event = AsyncEvent()
    instance.add_listener("timeout", set_event.set).start()

    await set_event.wait()

    instance.stop()

  @pytest.mark.timeout(7)
  @pytest.mark.asyncio
  async def test_timeout_reset(self):
    instance = Timeout(1.5)
    set_event = AsyncEvent()
    instance.add_listener("timeout", set_event.set)

    start_time = time.time()

    instance.start()

    Timeout(1).add_listener("timeout", instance.reset).start()
    Timeout(2).add_listener("timeout", instance.reset).start()
    Timeout(3).add_listener("timeout", instance.reset).start()
    Timeout(4).add_listener("timeout", instance.reset).start()
    Timeout(5).add_listener("timeout", instance.reset).start()

    await set_event.wait()

    instance.stop()

    assert time.time() - start_time > 6

  @pytest.mark.timeout(2)
  @pytest.mark.asyncio
  async def test_timeout_timedelta(self):
    instance = Timeout(timedelta(seconds=1))
    set_event = AsyncEvent()
    instance.add_listener("timeout", set_event.set).start()

    await set_event.wait()

    instance.stop()

  @pytest.mark.timeout(7)
  @pytest.mark.asyncio
  async def test_timeout_timedelta(self):
    instance = Timeout(timedelta(seconds=1.5))
    set_event = AsyncEvent()
    instance.add_listener("timeout", set_event.set)

    start_time = time.time()

    instance.start()

    Timeout(timedelta(seconds=1)).add_listener("timeout", instance.reset).start()
    Timeout(timedelta(seconds=2)).add_listener("timeout", instance.reset).start()
    Timeout(timedelta(seconds=3)).add_listener("timeout", instance.reset).start()
    Timeout(timedelta(seconds=4)).add_listener("timeout", instance.reset).start()
    Timeout(timedelta(seconds=5)).add_listener("timeout", instance.reset).start()

    await set_event.wait()

    instance.stop()

    assert time.time() - start_time > 6
